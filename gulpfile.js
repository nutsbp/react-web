// ==================== Module ====================
var gulp        = require('gulp');
var gulpUtil    = require('gulp-util');
var rename      = require('gulp-rename');
// Javascript
var browserify  = require('browserify');
var babelify    = require('babelify');
var vinylSource = require('vinyl-source-stream');
var vinylBuffer = require('vinyl-buffer');
var uglify      = require('gulp-uglify');
// ================================================

var path = {
    HTML: './src/index.html',
    ALL: ['./src/js/app.js', './src/js/*/*.js', 'src/index.html'],
    JS: ['./src/js/app.js', './src/js/*/*.js'],
    MINIFIED_OUT: 'app.min.js',
    DEST_SRC: 'dist/src',
    DEST_BUILD: 'dist/build',
    DEST: 'dist'
};

gulp.task('default', ['watch', 'browserify', 'uglify']);

gulp.task('browserify', () => {
    browserify({
        entries: ['./src/js/app.js'],   // Only need initial file, browserify finds the deps
        debug: false                     // Gives us sourcemapping
    })
    .transform(babelify, {presets: ["es2015", "react"]})
    .on('error', gulpUtil.log)
    .bundle()
    .pipe(vinylSource('bundle.js').on('error', gulpUtil.log))
    .pipe(vinylBuffer().on('error', gulpUtil.log))
    .pipe(gulp.dest('./src/js'));
});

gulp.task('uglify', () => {
    return gulp.src('./src/js/bundle.js')
    .pipe(rename(path.MINIFIED_OUT))
    .pipe(uglify().on('error', gulpUtil.log))
    .pipe(gulp.dest('./dist'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(path.JS, ['browserify', 'uglify']);
});
