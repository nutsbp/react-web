# Nutsbp editor React

## Dev

### url argv 说明
1) ?test: 所有 api 将使用 testdata 做处理 (no remote api for test)

### package version (app 版号) 意义

- major.minor.build-need (e.g. 1.0, 1.0.1)
- major 是最大的版本編號, minor 為其次, build 为小更新或 bug fixed.
- 更新版本请 Update CHANGELOG 文档
- need: hot-fix version (e.g. 1.0.3-a)

### git commit style: 方便阅读

- [add] 新增的功能 或 元件等等...
- [update] 更新的功能 或 元件等等...
- [delete] 删除的功能 或 元件等等...
- [fixed] 修正 bug or issue...
- [other] refactory or 整理 code等等...
- [public] new version.

### install

npm install npm install -g gulp-cli # use gulp build & watch

#### build & watch js change to bundle

npm run build

#### start develop server

npm run start

### prepare project

#### gulp

If you have previously installed a version of gulp globally, please run npm rm --global gulp to make sure your old version doesn't collide with gulp-cli.

1. npm install --global gulp-cli
2. [Start gulp]

3. Pull repository from [https://gto22834@bitbucket.org/nutsbp/react-web.git](mailto:https://gto22834@bitbucket.org/nutsbp/react-web.git)

4. Run package.json

  - npm install -> install package

#### install eslint plugin:

see more about [eslint-plugin-react]

- npm install eslint eslint-plugin-react --save-dev
- Add "plugins": ["react"] to your .eslintrc config file
- Add "ecmaFeatures": {"jsx": true} to your .eslintrc config file

### React Suite for react

[Amaze UI][cropper]

### learn more about gulp

1. [gulp]
2. [gulp-Vinyl][gulp-browserify-example]
3. [gulp高级技巧]
4. react-canvas 研究

### Back-end

1. pc 需要開 api 都要提早講
2. 如果手機版沒有此功能，pc 不會加

#### API

- Doc: <http://42.159.145.38:8888/>

#### more tech

1. [react-transform-boilerplate]

#### 生命週期

```javascript
<script type="text/babel">
  var Hello=React.createClass({
    getDefaultProps:function(){
        console.log("getDefaultProps, 1");
    },
    getInitialState:function(){
        console.log("getInitialState, 2");
        return null;
    },
    componentWillMount:function(){
        console.log("componentWillMount, 3");
    },
    render:function(){
        console.log("render, 4");
        return <p>Hi,LuckyWinty!</p>
    },
    componentDidMount:function(){
        console.log("componentDidMount, 5");
    },
  });
  React.render(<Hello></Hello>,document.body);
</script>
```

[amaze ui]: https://github.com/amazeui/amazeui-react
[cropper]: https://github.com/fengyuanchen/cropperjs#options
[eslint-plugin-react]: https://www.npmjs.com/package/eslint-plugin-react
[gulp]: https://github.com/gulpjs/gulp
[gulp-browserify-example]: http://blog.revathskumar.com/2016/02/browserify-with-gulp.html
[gulp-vinyl]: http://blog.revathskumar.com/2016/02/browserify-with-gulp.html
[gulp高级技巧]: https://csspod.com/advanced-tips-for-using-gulp-js/
[immutable.js]: https://github.com/facebook/immutable-js
[integrating react with gulp]: https://jonsuh.com/blog/integrating-react-with-gulp/
[radium]: https://github.com/FormidableLabs/radium
[radium-d3]: https://github.com/codesuki/react-d3-components
[react & react native 兼容]: https://github.com/tmallfe/tmallfe.github.io/issues/20
[react learn]: http://mikevalstar.com/post/fast-gulp-browserify-babelify-watchify-react-build/
[react mixins are dead]: https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750#.5c85j4aff
[react native css]: https://github.com/sabeurthabti/react-native-css
[react starterify]: https://github.com/Granze/react-starterify
[react-boilerplate框架]: https://github.com/mxstbr/react-boilerplate
[react-kendoui]: http://blog.jobbole.com/77703/
[react注意處]: https://segmentfault.com/a/1190000004105229
[react組件]: http://ant.design/
[redux]: https://github.com/reactjs/redux
[redux learn]: http://redux.js.org/index.html
[router學習]: http://web.jobbole.com/84294/
[start gulp]: https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md
[wildcards-asterisk-example]: http://gruntjs.com/configuring-tasks#globbing-patterns
[wildcards-asterisk-example2]: http://www.ydcss.com/archives/54
[react-component-concept]:https://camjackson.net/post/9-things-every-reactjs-beginner-should-know
[react-component-layout-learn]:https://github.com/broucz/react-inline-grid
[react-component-patterns]:https://github.com/planningcenter/react-patterns
[redux-in-chinese]:https://github.com/camsong/redux-in-chinese
[redux-tutorial]:https://github.com/react-guide/redux-tutorial-cn#redux-tutorial
