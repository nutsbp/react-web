import React from 'react';
import ReactDom from 'react-dom';
import Radium from 'radium';
import $ from 'jquery';

class Drag extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            dragging: false,
            left: 0,
            top: 0
        };
    }
    componentDidMount() {
        if (this.props.children) {
            let style = this.props.children.props.style;

            let dom = ReactDom.findDOMNode(this.refs.test);
            let offset = $(dom).offset();

            this.setState({
                left: offset.left,
                top: offset.top
            });
        }
    }
    onMouseDown(e) {
        this.setState({
            dragging: true
        });
    }
    onMouseUp(e) {
        this.setState({
            dragging: false
        });
    }
    onMouseMove(e) {
        if (!this.state.dragging) {
            return;
        }
        let left = e.pageX;
        let top = e.pageY;
        this.setState({
            left: left - 20,
            top: top - 20
        });
    }
    onMouseLeave(e) {
        this.setState({
            dragging: false
        });
    }
    render() {
        let props = {};
        if (this.state.top) {
            props.top = this.state.top + 'px';
        }
        if (this.state.left) {
            props.left = this.state.left + 'px';
        }
        return (
            <div
                onMouseDown={this.onMouseDown.bind(this)}
                onMouseUp={this.onMouseUp.bind(this)}
                onMouseMove={this.onMouseMove.bind(this)}
                onMouseLeave={this.onMouseLeave.bind(this)}
                onClick={()=>{console.error('drag click test');}}
            >
            {React.cloneElement(React.Children.only(this.props.children), {
                ref: 'test',
                style: Object.assign({}, this.props.children.props.style, props)
            })}
            </div>
        );
    }
}

export default Radium(Drag);