import React from 'react';

/**
 * 继承类 Component
 */

class Component extends React.Component {
    /** @override */
    constructor(prop) {
        super(prop);
    }
}

export default Component;