'use strict';
import React from 'react';
import Radium from 'radium';

var styleSheet = {
  btn: {
    position: 'absolute',
    width: '40px',
    height: '40px',
    top: '5px',
    right: '5px',
    textAlign: 'center',
    verticalAlign: 'middle',
    borderRadius: '10px',
    backgroundColor: '#F54F45',
    fontSize: '20px',
    fontWeight: 'bold',
    color: '#FFFFFF',
    lineHeight: '40px',
    cursor: 'pointer',
    ':active': {
      backgroundColor: '#B93A32'
    }
  }
};

class CloseButton extends React.Component {
  constructor(prop) {
    super(prop);
  }

  renderText(msg) {
    if (!msg) {
      return (<span style={{ fontFamily:'kustyle' }}>&#xe801;</span>);
    } else {
      return (<span style={{ lineHeight:'normal' }}>{msg}</span>);
    }
  }

  render() {
    var btn_name = this.props.children;
    var text = this.renderText(btn_name);
    var outer_style = this.props.style;
    // Basic main layout
    return (
      <div style={[ styleSheet.btn, outer_style ]} onClick={this.props.onButton}>
        {text}
      </div>
    );
  }
}

export default Radium(CloseButton);
