'use strict';
import React from 'react';

var styleSheet = {
  titleBox: {
    position: 'relative',
    width: '100%',
    height: '50px',
    lineHeight: '50px',
    backgroundColor: '#F6C76B'
  },
  title: {
    margin: '14px',
    fontSize: '22px',
    fontWeight: 'bold',
    color: '#FFFFFF'
  }
};

class PopupTitle extends React.Component {
  render() {
    var title = this.props.title;
    return (
      <div style={styleSheet.titleBox}>
        <span style={styleSheet.title}>{this.props.children}</span>
      </div>
    );
  }
}

export default PopupTitle;
