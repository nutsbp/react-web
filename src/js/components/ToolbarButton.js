'use strict';
import React from 'react';
import Radium from 'radium';

var styleSheet = {
  btn: {
    width: '80%',
    margin: '0 auto',
    padding: '20px 10px',
    textAlign: 'center',
    color: '#FFFFFF',
    cursor: 'pointer',
    border: '2px solid transparent',
    fontSize: '12px',
    ':active': {
      border: '2px solid #CBCBCB'
    }
  },
  icon: {
    fontFamily: 'kustyle',
    display: 'block',
    fontSize: '2em',
    pointerEvents: 'none'
  },
  name: {
    display: 'block',
    fontSize: '1em',
    marginTop: '1.5em',
    pointerEvents: 'none'
  }
};

class ToolbarButton extends React.Component {

  render() {
    var icon = this.props.children[0];
    var name = this.props.children[1];
    var outer_style = this.props.style;
    // Basic main layout
    return (
      <div style={[ styleSheet.btn, outer_style ]} onClick={this.props.onButton}>
        <span style={styleSheet.icon}>{icon}</span>
        <span style={styleSheet.name}>{name}</span>
      </div>
    );
  }
}

export default Radium(ToolbarButton);
