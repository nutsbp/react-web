'use strict';
import React from 'react';
import Radium from 'radium';

var styleSheet = {
  edge: {
    position: 'absolute',
    height: '50px',
    left: '20px',
    right: '20px',
    lineHeight: '50px',
    verticalAlign: 'middle',
    textAlign: 'center',
    color: '#5D5D5D',
    fontSize: '18px'
  },
  msg: {
    lineHeight: '1.25',
  }
};

class PopupMsg extends React.Component {
  render() {
    return (
      <div style={[ styleSheet.edge, this.props.style ]}>
        <span style={styleSheet.msg}>{this.props.children}</span>
      </div>
    );
  }
}

export default Radium(PopupMsg);
