'use strict';
import React from 'react';
import Radium from 'radium';

var styles = {
  background: {
    position: 'relative',
    width: '100%',
    height: '572px',
    top: '60px',
    overflow: 'hidden'
  },
  img: {
    position: 'absolute',
    height: '90%',
    top: '5%',
    left: '20px',
    border: '5px solid #303030',
    borderRadius: '10px'
  },
  title: {
    position: 'absolute',
    top: '5%',
    left: '400px',
    fontSize: '30px',
    fontWeight: 'bold',
    fontFamily: 'Microsoft YaHei'
  },
  intro: {
    position: 'absolute',
    top: '10%',
    left: '400px',
    margin: '20px 20px 20px 0',
    padding: '10px',
    backgroundColor: '#CBCBCB',
    fontSize: '20px',
    fontFamily: 'Microsoft YaHei'
  }
};

class GameIntro extends React.Component {
  renderTitle() {
    return (
      <div style={styles.title}>金錢遊戲3介紹</div>
    );
  }
  renderIntroText() {
    return (
      <div style={styles.intro}>
        金錢遊戲是一款模擬經濟體系的遊戲系統，初代由飯包一人獨力完成，緊接為了提供更好的體驗，成立小團隊開發二代及三代
      </div>
    );
  }
  render() {
    var title = this.renderTitle();
    var text = this.renderIntroText();
    // Basic main layout
    return (
      <section style={styles.background}>
        <img src="assets/images/intro_back.jpg" style={styles.img}/>
        {title}
        {text}
      </section>
    );
  }
}

export default Radium(GameIntro);
