'use strict';
import React from 'react';
import Radium from 'radium';

var styleSheet = {
  box: {
    position: 'relative',
    width: '70%',
    height: '45px',
    margin: '30px auto'
  },
  input: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    border: '2px solid #F6C76B',
    borderRadius: '10px',
    fontSize: '18px',
    fontWeight: 'bold',
    padding: '0 10px',
    background: 'none',
    boxSizing: 'border-box'
  }
};

class Input extends React.Component {
  getText() {
      return this.refs.input.value;
  }
  render() {
    var outer_style = this.props.style;
    var placeholder = this.props.placeholder;
    var onChange = this.props.inputChange;
    return (
      <div style={[ styleSheet.box, outer_style ]}>
        <input ref="input" style={styleSheet.input} placeholder={placeholder} onChange={onChange}></input>
      </div>
    );
  }
}

export default Radium(Input);
