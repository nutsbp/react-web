'use strict';
import React from 'react';
import Radium from 'radium';
import Drag from './Drag';

var styles = {
  btn: {
    position: 'absolute',
    width: '38px',
    height: '38px',
    top: '10px',
    right: '10px',
    textAlign: 'center',
    verticalAlign: 'middle',
    borderRadius: '2px',
    border: '1px solid #F6C76B',
    backgroundColor: '#303030',
    fontSize: '20px',
    fontWeight: 'bold',
    color: '#F6C76B',
    lineHeight: '38px',
    ':active': {
      border: '1px solid #E8B960',
      color: '#E8B960'
    }
  }
};

class Button extends React.Component {
  constructor(prop) {
    super(prop);
  }

  renderText(msg) {
    return (
      <span style={{ lineHeight:'normal' }}>{msg}</span>
    );
  }

  render() {
    var btn_name = this.props.children;
    var text = this.renderText(btn_name);
    var outer_style = this.props.style;
    // Basic main layout
    return (
      <Drag>
        <div style={[ styles.btn, outer_style ]} onClick={this.props.onButton}>{text}</div>
      </Drag>
    );
  }
}

export default Radium(Button);

// class A {
//   constructor(){
//     this._test='abc';
//   }
//   static go(arg){
//     console.log(`${arg} is a`);
//   }
//   inA(){
//     console.log('inA');
//   }
// }
//
// class B extends A{
//   constructor(){
//     super();
//     this._test2='abcd';
//   };
//   inA(){
//     super()
//   }
//   b(){
//
//   }
// }
//
// let bb=new B;
// A.go
// B.go
// bb.go
// // bb._test === 'abc'
// // bb._test2 === 'abcd'
// //
// let aa=new A;
// // aa._test==='abc';
// // aa._test2===undefined
// bb.inA();
