'use strict';
import React from 'react';
import Radium from 'radium';

let styles = {
  background: {
    position: 'relative',
    width: '100%',
    height: '580px',
    top: '51px',
    backgroundColor: '#CBCBCB',
    overflow: 'hidden'
  },
  title: {
    position: 'absolute',
    top: '5%',
    left: '400px',
    fontSize: '30px',
    fontWeight: 'bold',
    fontFamily: 'Microsoft YaHei'
  },
  intro: {
    position: 'absolute',
    top: '10%',
    left: '400px',
    margin: '20px 20px 20px 0',
    padding: '10px',
    backgroundColor: '#CBCBCB',
    fontSize: '20px',
    fontFamily: 'Microsoft YaHei'
  }
};

class ToolIntro extends React.Component {
  renderTitle() {
    return (
      <div style={styles.title}>瘋狂Bp介紹</div>
    );
  }
  renderIntroText() {
    return (
      <div style={styles.intro}>
        瘋狂Bp - 伴你一路到牛逼，馬上開始寫Bp
      </div>
    );
  }
  render() {
    var title = this.renderTitle();
    var text = this.renderIntroText();
    return (
      <section style={[ styles.background, this.props.style ]}>
        {title}
        {text}
      </section>
    );
  }
}

export default Radium(ToolIntro);
