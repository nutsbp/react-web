'use strict';
import React from 'react';
import Radium from 'radium';

var styleSheet = {
  btn: {
    position: 'absolute',
    width: '40px',
    height: '40px',
    borderRadius: '10px',
    backgroundColor: '#F54F45',
    fontSize: '18px',
    fontWeight: 'bold',
    color: '#FFFFFF',
    border: 'none',
    cursor: 'pointer',
    ':active': {
      backgroundColor: '#B93A32'
    }
  }
};

class PopupButton extends React.Component {
  render() {
    var btn_name = this.props.children || 'Button';
    var outer_style = this.props.style;
    // Basic main layout
    return (
      <button style={[ styleSheet.btn, outer_style ]} onClick={this.props.onButton}>
        <span>{btn_name}</span>
      </button>
    );
  }
}

export default Radium(PopupButton);
