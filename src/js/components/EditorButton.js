'use strict';
import React from 'react';
import Radium from 'radium';

var styleSheet = {
  btn: {
    position: 'relative',
    width: '100%',
    height: '50px',
    fontSize: '24px',
    color: '#FFFFFF',
    cursor: 'pointer',
    borderTop: 'none',
    borderRight: 'none',
    borderBottom: 'none',
    borderLeft: 'none',
    backgroundColor: '#303030',
    ':active': {
      backgroundColor: '#5D5D5D'
    }
  }
};

class EditorButton extends React.Component {

  render() {
    var btn_name = this.props.children;
    var outer_style = this.props.style;
    // Basic main layout
    return (
      <button style={[ styleSheet.btn, outer_style ]} onClick={this.props.onButton}>
        {btn_name}
      </button>
    );
  }
}

export default Radium(EditorButton);
