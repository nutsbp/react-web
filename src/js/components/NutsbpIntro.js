'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';
import { Link } from 'react-router';

import { show } from '../actions/UI.js';

var styleSheet = {
  background: {
    position: 'relative',
    width: '100%',
    height: '500px',
    top: '51px',
    backgroundColor: '#303030',
    overflow: 'hidden'
  },
  title: {
    position: 'relative',
    top: '10%',
    fontSize: '30px',
    fontWeight: 'bold',
    fontFamily: 'Microsoft YaHei',
    textAlign: 'center',
    color: '#F6C76B'
  },
  intro: {
    position: 'relative',
    top: '15%',
    fontSize: '20px',
    fontFamily: 'Microsoft YaHei',
    textAlign: 'center',
    color: '#CBCBCB'
  },
  img: {
    position: 'absolute',
    width: '30%',
    left: '35%',
    top: '30%',
    borderRadius: '10px',
  },
  btn: {
    position: 'absolute',
    width: '40%',
    height: '60px',
    bottom: '15%',
    left: '30%',
    border: 'none',
    borderRadius: '10px',
    fontSize: '20px',
    fontFamily: 'Microsoft YaHei',
    color: '#FFFFFF',
    backgroundColor: '#F54F45',
    ':active': {
      backgroundColor: '#B93A32'
    }
  }
};

class NutsbpIntro extends React.Component {
  renderTitle() {
    return (
      <div style={styleSheet.title}>瘋狂Bp介紹</div>
    );
  }

  renderIntroText() {
    return (
      <div style={styleSheet.intro}>
        瘋狂Bp - 伴你一路到牛逼，馬上開始寫Bp
      </div>
    );
  }

  render() {
    var title = this.renderTitle();
    var text = this.renderIntroText();
    return (
      <section style={styleSheet.background}>
        {title}
        {text}
        <img src="assets/images/bp_intro.jpg" style={styleSheet.img}/>
        <Link to="editor">
            <button style={styleSheet.btn}>開始創建 BP</button>
        </Link>
      </section>
    );
  }

  onCreateBp() {
    this.props.dispatch(show('Editor', 'open editor frame'));
  }
}

export default connect(NutsbpIntro.onCreateBp)(Radium(NutsbpIntro));
