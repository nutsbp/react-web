import React from 'react';
import { Router, Route, hashHistory } from 'react-router';
import Root from '../containers/Root';

import Login from '../containers/Login.js';
import SignUp from '../containers/SignUp.js';
import Confirm from '../containers/Confirm.js';
import Message from '../containers/Message.js';
import Loading from '../containers/Loading.js';
import Editor from '../containers/Editor';
import EditorMain from '../containers/editor/Main';

let requireCredentials = (nextState, replace, next) => {
    const query = nextState.location.query;
    if (nextState) { // TODO: 权限检查
        next();
    } else {
        replace('/');
        next();
    }
};

export default(
    <Router history={hashHistory}>
        <Route path="/" component={Root}>
            <Route path="/login" component={Login}/>
            <Route path="/signup" component={SignUp}/>
            <Route path="/confirm" component={Confirm}/>
            <Route path="/message" component={Message}/>
            <Route path="/loading" component={Loading}/>
        </Route>
        <Route path="/editor" component={Editor} onEnter={requireCredentials}></Route>
        <Route path="/nutsbpeditor" component={EditorMain}></Route>
    </Router>
);
