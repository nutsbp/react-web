import $ from 'jquery';
import NUTS from '../libs/NUTS';

let _cacheHtml = [];
let _cacheCss = [];

export const loadTemplate = (options, callback, noCache) => {
    options = options || {};
    callback = callback || function () {};
    noCache = noCache || false;
    var templateConfig = options.templateConfig || {};
    var template_id = options.template_id || 1;
    var template_step = options.template_step || 1;
    var layout = NUTS.find(template_id + '.' + template_step + '.layout', templateConfig) || 1;
    var maxCount = NUTS.find(template_id + '.' + template_step + '.count', templateConfig) || 1;
    var itemCount = options.itemCount || 1;

    var path = '../../template/' + template_id + '/';
    var filename = 'bp' + template_id + '-' + template_step;

    var loadHtml = function () {
        if (!noCache && NUTS.find(filename, _cacheHtml)) {
            loadHtmlAfter(NUTS.find(filename, _cacheHtml));
            return;
        }
        // ios app (iphone6 plus) 不知为何 需要这样才能解决
        setTimeout(function () {
            $.get(path + 'html/' + filename + '.html', function (res) {
                NUTS.declare(filename, res, _cacheHtml);
                loadHtmlAfter(res);
            });
        }.bind(this), 100);
    };

    var section;
    var sectionID;
    var loadHtmlAfter = function (res) {
        var div = document.createElement('div');
        div.innerHTML = res;
        if (!maxCount || maxCount === 1) {
            section = div.querySelector('section');
        } else {
            sectionID = '#NED-bp' + template_id + '-' + template_step + '-' + layout + '-';
            sectionID += 'item' + itemCount;
            section = div.querySelector(sectionID);
        }
        var imgs = Array.prototype.slice.call(section.querySelectorAll('img'));
        imgs.forEach(function (el) {
            el.onload = function () {};
            el.onerror = function () {
                var src = el.src;
                el.src = "template/x.gif";
                el.src = src;
            };
        });
        callback(section);
    }.bind(this);

    // common template css:
    loadCss(path + 'css/' + filename + '.css', noCache, function () {
        // template css:
        loadCss(path + 'css/bp' + template_id + '.css', noCache, function () {
            loadHtml();
        });
    }.bind(this));
};


const _loadCss = function (url, onload, tryCount) {
    onload = onload || function () {};
    tryCount = tryCount || 0;
    var link = document.createElement("link");
    var count = document.styleSheets.length + 1;
    var id = "load-css-" + count;
    link.setAttribute('id', id);
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');
    link.setAttribute('href', url);
    document.getElementsByTagName("head")[0].appendChild(link);
    link.onload = function () {
        onload(url);
    };
    link.onerror = function () {
        if (tryCount >= 10) { // try 10 次
            onload(url);
            return;
        }
        tryCount++;
        _loadCss(url, onload, tryCount);
    };
};

export const loadCss = (filePath, noCache, onload) => {
    onload = onload || function () {};
    if (!noCache) {
        let loaded = false;
        _cacheCss.forEach(function (css) {
            if (css === filePath) {
                loaded = true;
            }
        });
        if (loaded) {
            onload();
            return;
        }
        _cacheCss.push(filePath);
    }
    _loadCss(filePath, onload);
};