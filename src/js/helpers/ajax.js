import fetch from 'isomorphic-fetch';
import { parseJSON, checkHttpStatus } from '../helpers/http';

let buildQueryString =function (obj) {
    var key;
    var value;
    var isArray = function(obj) {
        return Array.isArray(obj) || (typeof obj === 'object' && Object.prototype.toString.call(obj) === '[object Array]');
    };
    var isObject  = function(obj) {
        return typeof obj === "object" && Object.prototype.toString.call(obj) === '[object Object]';
    };
    var buildPair = function (key, value) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(value);
    };
    var buildPairs = function recursive(params, prefix, pairs) {
        var pre;
        if (isObject(params)) {
            var key;
            var value;
            for (key in params) {
                if (params.hasOwnProperty(key)) {
                    value = params[key];
                    if (prefix === '') {
                        pre = key;
                    } else {
                        pre = prefix + '[' + key + ']';
                    }
                    if (typeof value === 'object') {
                        if(value === null){
                            pairs.push(buildPair(pre, value));
                        }else{
                            pairs = recursive(value, pre, pairs);
                        }
                    } else {
                        pairs.push(buildPair(pre, value));
                    }
                }
            }
        } else if (isArray(params)) {
            pre = prefix + '[]';
            params.forEach(function (value, i) {
                if (typeof value === 'object') {
                    if(value===null){
                        pairs.push(buildPair(pre, value));
                    }else{
                        if(typeof value==='object'){
                            pre=prefix+'['+i+']';
                        }
                        pairs = recursive(value, pre, pairs);
                    }
                } else {
                    pairs.push(buildPair(pre, value));
                }
            });
        }
        return pairs;
    };
    return buildPairs(obj, '', []).join('&');
};

export const custom = (method, url, data, dataType, success, failure) => {
    method = method || "GET";
    success = success || function () {};
    failure = failure || function () {};
    let params = {
        method: method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    };
    if (method==='GET' || method==='DELETE' || method==='PUT') {
        if (data) {
            url = url + '?' + buildQueryString(data);
        }
    } else {
        // request payload
        // var formdata = new FormData();
        // var idx;
        // for (idx in data) {
        //     formdata.append(idx, data[idx]);
        // }

        // form data
        params.body = buildQueryString(data);
        // JSON params
        // JSON.stringify(data);
    }

    if (dataType === 'json') {
        fetch(url, params)
        .then(checkHttpStatus)
        .then(parseJSON)
        .then(json => {
            if (json && parseInt(json.code) === 0) {
                success(json);
            } else {
                failure(json);
            }
        })
        .catch(error => {
            failure(error);
        });
        return;
    }
    fetch(url, params)
    .then(checkHttpStatus)
    .then(response => {
        success(response);
    })
    .catch(error => {
        failure(error);
    });
};

export function GET (url, data, success, failure, dataType) {
    data = data || {};
	return custom('GET', url, data, dataType, success, failure);
}

export function POST (url, data, success, failure, dataType) {
	return custom('POST', url, data, dataType, success, failure);
}

export function PATCH (url, data, success, failure, dataType) {
	return custom('PATCH', url, data, dataType, success, failure);
}

export function DELETE (url, data, success, failure, dataType) {
	return custom('DELETE', url, data, dataType, success, failure);
}