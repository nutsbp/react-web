import NUTS from './libs/NUTS';

let _cache = {};

module.exports = class Main {
    static set(path_or_data, data) {
        if (data) {
            NUTS.declare(path_or_data, data, _cache);
        } else {
            _cache = Object.assign({}, path_or_data, _cache);
        }
    }
    static get(path) {
        if (typeof path === 'undefined') {
            return _cache;
        }
        return NUTS.find(path, _cache);
    }
};