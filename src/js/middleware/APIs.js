import fetch from 'isomorphic-fetch';
import { custom } from '../helpers/ajax';
import { parseJSON, checkHttpStatus } from '../helpers/http';


export function GET (url, data, success, failure) {
    data = data || {};
    if (url.match(/42.159.145.38:3000/)) {
        data.u = "igdtvxbiw0wv20130801000000aB58cE85320f4772";
        data.v = "1.0.35";
    }
	return custom('GET', url, data, 'json', success, failure);
}

export function POST (url, data, success, failure) {
    if (url.match(/42.159.145.38:3000/)) {
        data.u = "igdtvxbiw0wv20130801000000aB58cE85320f4772";
        data.v = "1.0.35";
    }
	return custom('POST', url, data, 'json', success, failure);
}

export function PATCH (url, data, success, failure) {
    if (url.match(/42.159.145.38:3000/)) {
        data.u = "igdtvxbiw0wv20130801000000aB58cE85320f4772";
        data.v = "1.0.35";
    }
	return custom('PATCH', url, data, 'json', success, failure);
}

export function DELETE (url, data, success, failure) {
	return custom('DELETE', url, data, 'json', success, failure);
}

/**
 * 接口地址类
 * @create: 2016年06月30日
 */
export const HTTP = "http://";
export const HTTPS = "https://";
export const URL_SPLITTER = "/";

export const API_HOST_DEBUG = "api.bp.nutsb.com";
export const API_HOST = "api.nutsbp.com";
export const API_RESTFUL_HOST = "http://42.159.145.38:3000/";

export const HOME_PAGE = "http://www.nutsbp.com";
export const HOME_PAGE_DEBUG = "http://www.bp.nutsb.com";
export const DOWNLOAD_PAGE = "http://www.nutsbp.com/download/app";
export const AGREEMENT = "http://www.nutsbp.com/html/user/register-agreement-mobile.html\n";
export const BPTEMPLATE_URL = "http://42.159.145.38/dev";
export const BPTEMPLATE_COVER_URL = "http://42.159.145.38/template/%1$s/index.jpg";
export const BPTEMPLATE_TEMPLATE_PREVIEW = "http://42.159.145.38/?action=previewTemplate&template_id=%1$s";
export const BPTEMPLATE_SHARE_URL = "http://42.159.145.38/?action=shareurl&slide_id=%1$s";
export const INDUSTRY_ITEMS_URL = "http://42.159.145.38/lang/zh-cn/title/%1$s.json";
export const BPTEMPLATE_EXAMPLE_URL = "http://42.159.145.38/lang/zh-cn/example/%1$s.json";
export const INDUSTRY_ADVENTAGE_URL = "http://42.159.145.38/lang/zh-cn/title/adventage.json";
export const INDUSTRY_MILESTONE_URL = "http://42.159.145.38/lang/zh-cn/title/result.json";

// TODO switch PROD / DEV URL
export const URL_API_HOST = HTTP + API_HOST_DEBUG + URL_SPLITTER;

//Common API
export const API_LOGIN = URL_API_HOST + "user/signin";                      //登录接口
export const API_VERIFICATION = URL_API_HOST + "util/sms/verifyCode";       //发送验证码
export const API_REGISTER = URL_API_HOST + "user/signin";                   //注册接口
export const API_UPDATE_PROFILE = URL_API_HOST + "user/profile";            //修改个人资料
export const API_COUNT_ACTION = URL_API_HOST + "ops/tub";                   //事件统计
export const API_MSG_COUNT = URL_API_HOST + "amo/nutify";                   //消息统计
export const API_UPLOAD_AVATAR = URL_API_HOST + "upload/userAvatar";        //上传头像
export const API_VERSION = URL_API_HOST + "app/version";                    //版本更新接口
export const API_RESET_PASSWORD = URL_API_HOST + "user/resetPassword";      //重置密码接口
export const API_CHANGE_PASSWORD = URL_API_HOST + "user/security/password"; //修改密码接口
export const API_FEEDBACK = URL_API_HOST + "amo/feedback";                  //用户反馈
export const API_VERSIONCHECK = URL_API_HOST + "/amo/nutsbp";               //版本检查
export const API_TALK_RECENT = URL_API_HOST + "nutalk/recent";              //最近消息列表
export const API_TALK_MSGLIST = URL_API_HOST + "nutalk/window";             //个人对话列表
export const API_TALK_SEND = URL_API_HOST + "nutalk/anota";                 //发送留言

//Beginner Api
export const API_GET_BP_BRIEF = URL_API_HOST + "uipage/pioneer/mybp/myBpPage";                  //获得个人BP信息
export const API_SET_SHARE = URL_API_HOST + "pioneer/mybp/bpShareLink";                         //开关BP分享接口
export const API_GET_BP_DETAIL = URL_API_HOST + "pioneer/mybp/myBp";                            //获得BP详情
export const API_PERSONAL_MYPROFILE = URL_API_HOST + "user/profile";                            //我的详细页面
export const API_GET_BANNERLIST = URL_API_HOST + "uipage/pioneer/mybp/banner";                  //首页banner
export const API_GET_INVESTOR = URL_API_HOST + "pioneer/sendbp/hunter";                         //投资人列表
export const API_GET_INVESTOR_DETAIL = URL_API_HOST + "pioneer/bookmark/hunter";                //投资人详情
export const API_BP_DELIVER = URL_API_HOST + "pioneer/sendbp/sendBp";                           //投递BP
export const API_GET_MAIL_LIST = URL_API_HOST + "bp/deliver/bymail/investorLine";               //获取投资人邮箱列表
export const API_GET_MAIL_INDUSTRY = URL_API_HOST + "bp/deliver/bymail/getIndustryClassify";    //获取投资人邮箱行业分类
export const API_GET_HISTORY_LIST = URL_API_HOST + "pioneer/sendbp/sendBp";                     //投递历史
export const API_INVESTOR_COLLECT = URL_API_HOST + "pioneer/bookmark/hunter";                   //投资人收藏

//Investor Api
export const API_INVESTOR_INVEST_PAGE = URL_API_HOST + "uipage/hunter/me/shinePage";        //投资人信息
export const API_INVESTOR_INVEST_PREFER = URL_API_HOST + "user/hunter/shinePrefer";         //投资人偏好信息
export const API_INVESTOR_INVEST_CASE = URL_API_HOST + "user/hunter/shineCase";     //投资人投资案例
export const API_GET_INVESTOR_BANNERLIST = URL_API_HOST + "uipage/hunter/discover/banner";  //投资人banner
export const API_INVESTOR_FOLDER_BPS = URL_API_HOST + "hunter/bpbox/box";           //文件夹BP列表
export const API_INVESTOR_DUSTBIN = URL_API_HOST + "hunter/bpbox/trash";            //垃圾箱操作
export const API_INVESTOR_BP = URL_API_HOST + "hunter/bpbox/abp";                   //投资人端BP
export const API_COLLECT_BEGINNER = URL_API_HOST + "hunter/bookmark/pioneer";       //收藏创业者
export const API_COLLEC_BEGINNER_DETAIL = URL_API_HOST + "hunter/bookmark/pioneer"; //创业者详情
export const API_COLLECT_INVESTOR = URL_API_HOST + "hunter/bookmark/hunter";        //收藏的投资人
export const API_COLLECT_BEGINNER_LIST = URL_API_HOST + "hunter/bookmark/pioneer";  //收藏的创业者列表
export const API_FOLDER_COUNT = URL_API_HOST + "hunter/bpbox/category";             //分类文件夹BP数目
export const API_FOLDER_NOTICE = URL_API_HOST + "amo/nutify";                       //小红点
export const API_POOL_BPLIST = URL_API_HOST + "hunter/discover/fairpool/pool";      //项目池BP列表
export const API_POOL_BP = URL_API_HOST + "hunter/discover/fairpool/abp";           //项目池BP

//Restful Api
export const API_RESTFUL_GET_PROJECT = API_RESTFUL_HOST + "project";                //获取我的项目
export const API_RESTFUL_SLIDE = API_RESTFUL_HOST + "slide";                        //BP相关
export const API_RESTFUL_TEMPLATE = API_RESTFUL_HOST + "template";                  //模板相关
export const API_RESTFUL_CONFIG = API_RESTFUL_HOST + "config";                      //获取app配置
export const API_RESTFUL_IMAGE_UPLOAD = URL_API_HOST + "upload/normal";             //上传图片
export const API_RESTFUL_AVATAR_UPLOAD = URL_API_HOST + "upload/avatar";            //上传头像
export const API_RESTFUL_USER_AVATAR_UPLOAD = URL_API_HOST + "upload/userAvatar";   //上传使用者头像