import React from 'react';
import Radium from 'radium';
import { connect } from 'react-redux';

class Main extends React.Component {
    render() {
        return (
            <div id="ne-wrap" onClick={(e)=>{console.error(e);}}>
                <div id="ne-section">
                    <div className="ne-sectionTable">
                    </div>
                </div>
            </div>
        );
    }
}

export default connect()(Radium(Main));
