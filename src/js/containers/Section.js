'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';

var _oriSectionSize = [640, 960];

var styleSheet = {
  background: {
    position: 'absolute',
    width: '640px',
    height: '960px',
    top: '50%',
    left: '50%',
    backgroundColor: '#FFFFFF'
  }
};

class Section extends React.Component {
  constructor() {
    super();
  }
  /** overwrite */
  render() {
    console.error(this.props);
    return (
      <div style={[ styleSheet.background, this.props.style ]} ref="slideWrap">

      </div>
    );
  }
  /** overwrite */
  componentDidMount() {

  }
}

export default connect()(Radium(Section));
