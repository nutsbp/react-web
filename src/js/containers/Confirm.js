'use strict';
import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { show, hide } from '../actions/UI.js';

import Radium from 'radium';

import PopupTitle from '../components/PopupTitle.js';
import PopupMsg from '../components/PopupMsg.js';
import PopupBtn from '../components/PopupButton.js';

let styleSheet = {
  background: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  },
  content: {
    position: 'absolute',
    height: '50%',
    right: '50px',
    left: '50px',
    top: '100px',
    borderRadius: '10px',
    backgroundColor:'#EBEBEB',
    overflow: 'hidden'
  },
  text: {
    height: '150px',
    lineHeight: '150px',
    top: '70px'
  },
  btnConfirm: {
    position: 'absolute',
    width: '30%',
    height: '60px',
    bottom: '40px',
    left: '15%',
    color: '#FFFFFF',
    backgroundColor: '#8CCB59',
    ':active': {
      backgroundColor: '#6A9A42',
    }
  },
  btnCancel: {
    position: 'absolute',
    width: '30%',
    height: '60px',
    bottom: '40px',
    right: '15%',
    color: '#FFFFFF',
    backgroundColor: '#F54F45',
    ':active': {
      backgroundColor: '#B93A32',
    }
  }
};

class Confirm extends React.Component {
  render() {
    if(this.props.display === 'Confirm') {
      styleSheet.background.display = 'block';
      var text = this.props.msg;
      return (
        <div style={styleSheet.background}>
          <div style={styleSheet.content}>
            <PopupTitle>Confirm</PopupTitle>
            <PopupMsg style={styleSheet.text}>{text}</PopupMsg>
            <PopupBtn style={styleSheet.btnConfirm} onButton={()=>this.onConfirm()}>
              Confirm
            </PopupBtn>
            <PopupBtn style={styleSheet.btnCancel} onButton={()=>this.onClose()}>Cancel</PopupBtn>
          </div>
        </div>
      );
    } else {
      styleSheet.background.display = 'none';
      return null;
    }
  }

  onClose() {
    this.props.dispatch(hide(null, null));
  }

  onConfirm () {
    console.error('Confirm');
    // this.props.dispatch(show('Confirm', 'open Confirm frame'));
  }
}

export default connect(Confirm.onClose, Confirm.onConfirm)(Radium(Confirm));
