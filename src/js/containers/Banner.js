'use strict';
import React from 'react';
import Radium from 'radium';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { show, hide } from '../actions/UI.js';
import { logout } from '../actions/Login';

// TODO test action (will remove)
import { fetchSlide, createSlide, updateSlide, fetchSlideList } from '../actions/Slide';
import { fetchProject } from '../actions/Project';
import { fetchBpBrief, fetchBpShareLink } from '../actions/Bp';

import Button from '../components/Button.js';

var styles = {
  background: {
    position: 'fixed',
    width: '100%',
    height: '50px',
    top: 0,
    borderBottom: '1px solid #CBCBCB',
    backgroundColor: '#303030'
  },
  btn: {
    top: '5px',
    right: '5px',
    border: 'none',
    fontFamily: 'kustyle'
  },
  loginBtm: {
    width: '50px',
    height: '30px',
    top: '10px',
    right: '60px',
    lineHeight: '30px'
  },
  logoutBtm: {
    width: '50px',
    height: '30px',
    top: '10px',
    right: '60px',
    lineHeight: '30px',
    display: 'none'
  }
};

import { fetchBpBanner } from '../actions/Bp';
class Banner extends React.Component {

  render() {
    let testBtm = Object.assign({}, styles.loginBtm);
    testBtm.right = "120px";
    testBtm.width = "80px";
    let loginBtm = Object.assign({}, styles.loginBtm);
    let logoutBtm = Object.assign({}, styles.logoutBtm);
    let btn = Object.assign({}, styles.btn);
    if (this.props.isAuthenticating) {
        loginBtm.color = 'gray';
    }
    if (this.props.isAuthenticated) {
        loginBtm.display = 'none';
        logoutBtm.display = 'block';
    }
    return (
      <div style={styles.background}>
        <Link to="nutsbpeditor"><Button style={testBtm}>编辑器</Button></Link>
        <Link to="login"><Button style={loginBtm}>登入</Button></Link>
        <Button style={logoutBtm} onButton={this.onLogout.bind(this)}>登出</Button>
        <Button style={btn} onButton={this.onOpenOperation.bind(this)}>&#xe800;</Button>
      </div>
    );
  }

  onOpenLogin() {
    console.error('open login');
    this.props.dispatch(show('login', 'Show login popup'));
  }

  onOpenOperation() {
    console.error('open operation');
    this.props.dispatch(fetchProject());
  }
  onLogout() {
    this.props.dispatch(logout());
  }
}

// const selector = (dispatch) => {
//   return {
//     test: 'test'
//   };
// };
const mapStateToProps = (state, props) => {
    return {
        isAuthenticating: state.Auth.isAuthenticating,
        isAuthenticated: state.Auth.isAuthenticated,
        statusText: state.Auth.statusText
    };
};
export default connect(mapStateToProps)(Radium(Banner));
