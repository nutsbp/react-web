'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';

import EditorButton from '../components/EditorButton.js';

var styleSheet = {
  wrap: {
    position: 'absolute',
    width: '40px',
    backgroundColor: 'rgba(48, 48, 48, 0.7)'
  },
  btn: {
    height: '40px',
    fontFamily: 'kustyle',
    fontSize: '20px',
    color: '#F6C76B'
  }
};

class SlideOptions extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div style={[ styleSheet.wrap, this.props.style ]}>
        <EditorButton style={styleSheet.btn}>&#xe81D;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe81C;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe820;</EditorButton>
      </div>
    );
  }
}

export default connect()(Radium(SlideOptions));
