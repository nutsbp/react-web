'use strict';
import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { show, hide } from '../actions/UI.js';

import Radium from 'radium';

import PopupTitle from '../components/PopupTitle.js';
import PopupMsg from '../components/PopupMsg.js';
import PopupBtn from '../components/PopupButton.js';

let styleSheet = {
  background: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    backgroundColor: '#5D5D5D'
  },
  text: {
    position: 'absolute',
    width: '100%',
    height: '40px',
    top: '50%',
    marginTop: '-20px',
    fontSize: '36px',
    fontWidth: 'bold',
    color: '#F6C76B',
    textAlign: 'center'
  }
};

class Loading extends React.Component {
    render() {
        if (this.props.display === 'Loading') {
            return (
                <div style={styleSheet.background}>
                  <div style={styleSheet.text}>Loading...</div>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default connect()(Radium(Loading));
