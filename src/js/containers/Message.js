'use strict';
import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { show, hide } from '../actions/UI.js';

import Radium from 'radium';

import PopupTitle from '../components/PopupTitle.js';
import PopupMsg from '../components/PopupMsg.js';
import PopupBtn from '../components/PopupButton.js';

let styleSheet = {
  background: {
    position: 'fixed',
    zIndex: 999999,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  },
  content: {
    position: 'absolute',
    height: '50%',
    right: '50px',
    left: '50px',
    top: '100px',
    borderRadius: '10px',
    backgroundColor:'#EBEBEB',
    overflow: 'hidden'
  },
  text: {
    height: '150px',
    lineHeight: '150px',
    top: '70px'
  },
  btnCancel: {
    position: 'absolute',
    width: '30%',
    height: '60px',
    bottom: '40px',
    right: '35%',
    color: '#FFFFFF',
    backgroundColor: '#59C1CB',
    ':active': {
      backgroundColor: '#42929A',
    }
  }
};

class Message extends React.Component {
    render() {
        if(this.props.display === 'Message') {
            var text = this.props.msg || '帳號密碼錯誤';
            return (
                <div style={styleSheet.background}>
                    <div style={styleSheet.content}>
                        <PopupTitle>Message</PopupTitle>
                        <PopupMsg style={styleSheet.text}>{text}</PopupMsg>
                        <PopupBtn style={styleSheet.btnCancel} onButton={()=>this.onClose()}>OK</PopupBtn>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }

    onClose() {
        this.props.dispatch(hide(null, null));
    }
}

export default connect()(Radium(Message));
