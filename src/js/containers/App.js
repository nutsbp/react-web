import React, { Component } from 'react';
import Footer from '../containers/Footer';
import Main from '../containers/Main';
import { Link } from 'react-router';

class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <ul role="nav">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/about">About</Link></li>
                    <li><Link to="/main">Main</Link></li>
                    <li><Link to="/login">Login</Link></li>
                </ul>
            <div>{this.props.children}</div>
            <Footer />
            </div>
        );
    }
}

export default App;
