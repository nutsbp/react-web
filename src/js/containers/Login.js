'use strict';
import React from 'react';
import { connect } from 'react-redux';
import { show, hide } from '../actions/UI.js';
import { loginUser } from '../actions/Login';
import { Link, hashHistory } from 'react-router';
import Radium from 'radium';

import PopupTitle from '../components/PopupTitle.js';
import CloseBtn from '../components/CloseButton.js';
import Input from '../components/Input.js';
import PopupBtn from '../components/PopupButton.js';
import Loading from './Loading';

let styleSheet = {
  background: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  },
  content: {
    position: 'absolute',
    height: '60%',
    right: '50px',
    left: '50px',
    top: '100px',
    borderRadius: '10px',
    backgroundColor:'#EBEBEB',
    overflow: 'hidden'
  },
  input: {
    top: '20px'
  },
  btnLogin: {
    position: 'absolute',
    width: '30%',
    height: '60px',
    bottom: '40px',
    left: '15%',
    color: '#FFFFFF',
    backgroundColor: '#8CCB59',
    ':active': {
      backgroundColor: '#6A9A42',
    }
  },
  btnRegister: {
    position: 'absolute',
    width: '30%',
    height: '60px',
    bottom: '40px',
    right: '15%',
    color: '#FFFFFF',
    backgroundColor: '#59C1CB',
    ':active': {
      backgroundColor: '#42929A',
    }
  }
};

class Login extends React.Component {
    render() {
        if (this.props.isAuthenticating) {
            return (<Loading></Loading>);
        }
        return (
            <div style={styleSheet.background}>
                <div style={styleSheet.content}>
                    <PopupTitle>Login</PopupTitle>
                    <CloseBtn onButton={this.onClose}></CloseBtn>
                    <Input ref="mobile" style={styleSheet.input} placeholder="請輸入帳號"></Input>
                    <Input ref="password" style={styleSheet.input} placeholder="請輸入密碼"></Input>

                    <PopupBtn style={styleSheet.btnLogin} onButton={()=>{
                        this.props.onLogin(this.refs.mobile.getText(), this.refs.password.getText());
                    }}>Login</PopupBtn>

                    <Link to="signup">
                        <PopupBtn style={styleSheet.btnRegister}>
                            Register
                        </PopupBtn>
                    </Link>
                </div>
            </div>
        );
    }
    onClose() {
        hashHistory.goBack();
    }
}

const mapStateToProps = (state, props) => {
    return {
        isAuthenticating: state.Auth.isAuthenticating
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onLogin: (mobile, password) => {
            dispatch(hide(null, null));
            if (!mobile || !password) {
                dispatch(show('Message', '请输入帐号密码'));
                return;
            }
            dispatch(loginUser(mobile, password));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Radium(Login));
