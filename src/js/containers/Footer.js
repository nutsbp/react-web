'use strict';
import React from 'react';
import Radium from 'radium';
import { connect } from 'react-redux';

var styles = {
  background: {
    position: 'fixed',
    width: '100%',
    height: '50px',
    bottom: 0,
    borderTop: '1px solid #CBCBCB',
    backgroundColor: '#303030'
  },
  contactUs: {
    color: '#FFFFFF',
    fontSize: '20px',
    fontWeight: 'bold',
    lineHeight: '50px',
    margin: '0 0 0 10px'
  }
};

class Footer extends React.Component {
  renderContactUs() {
    return (
      <span style={styles.contactUs}>聯絡方式</span>
    );
  }

  render() {
    var infoElm = this.renderContactUs();
    // Basic main layout
    return (
      <div style={styles.background}>
        {infoElm} {this.props.ENV}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    ENV: state.Config.ENV
  };
};
export default connect(mapStateToProps)(Radium(Footer));
