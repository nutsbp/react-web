'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';
import Section from './Section.js';

import { fetchSlideWrap } from '../actions/Slide';

var _oriSectionSize = [640, 960];

var styleSheet = {
  wrap: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: '240px'
  },
  section: {
    position: 'absolute',
    width: '640px',
    height: '960px',
    top: '50%',
    left: '50%',
    backgroundColor: '#FFFFFF'
  }
};

class Editor extends React.Component {
  constructor() {
    super();
  }
  renderSection () {

  }
  /** overwrite */
  render() {
    return (
      <div style={styleSheet.wrap} ref="slideWrap">
        <Section style={styleSheet.section} wrap={this.props}></Section>
      </div>
    );
  }
  /** overwrite */
  componentDidMount() {
    this.setScreen();
  }
  /**
   * get wrap size for section
   * @param {object}         elm  The element of wrap
   * @param {string | array} size The size of custom
   */
  setScreen(elm, size) {
    var slide_wrap_elm = this.refs.slideWrap;
    var wrap_size = [slide_wrap_elm.offsetWidth, slide_wrap_elm.offsetHeight];
    var ori_size = _oriSectionSize;
    var scale_w = wrap_size[0] / ori_size[0];
    var scale_h = wrap_size[1] / ori_size[1];
    if(size && size !== 'fill') {
      // TODO: 設定指定 size
    } else if (size === 'fill') {
      // TODO: 設定填滿模式
    } else {
      // set full screen size
      if(scale_w > scale_h) {
          scale_w = scale_h;
      } else if(scale_h > scale_w) {
          scale_h = scale_w;
      }
    }

    this.props.dispatch(fetchSlideWrap({
      wrapElm: slide_wrap_elm,
      size: _oriSectionSize,
      scale: [scale_w, scale_h]
    }));
  }
}

const readySlide = (state, props) => {
  return {
    wrap: state.Slide
  };
};

export default connect(readySlide, Editor.setScreen)(Radium(Editor));
