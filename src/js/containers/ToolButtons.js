'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';

import ToolbarButton from '../components/ToolbarButton.js';

var styleSheet = {
  wrap: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    overflow: 'hidden'
  },
  tool_list: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflowY: 'auto',
    overflowX: 'hidden',
    padding: '12px'
  },
  btn: {
    fontFamily: 'kustyle',
    borderBottom: '1px solid #CBCBCB'
  }
};

class Toolbar extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div style={styleSheet.wrap}>
        <div style={styleSheet.tool_list}>
          <ToolbarButton>&#xe80c;{'Text'}</ToolbarButton>
          <ToolbarButton>&#xe811;{'Image'}</ToolbarButton>
          <ToolbarButton>&#xe812;{'Bar'}</ToolbarButton>
          <ToolbarButton>&#xe813;{'Line'}</ToolbarButton>
          <ToolbarButton>&#xe814;{'Circle'}</ToolbarButton>
          <ToolbarButton>&#xe815;{'Table'}</ToolbarButton>
        </div>
      </div>
    );
  }
}

export default connect()(Radium(Toolbar));
