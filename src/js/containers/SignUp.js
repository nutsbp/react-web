'use strict';
import React from 'react';
import { connect } from 'react-redux';
import { Link, hashHistory } from 'react-router';
import { hide } from '../actions/UI.js';
import { registerUser, registerGetVerifyCode, decrementVerifyCodeExpireTime } from '../actions/Login.js';

import Radium from 'radium';

import PopupTitle from '../components/PopupTitle.js';
import CloseBtn from '../components/CloseButton.js';
import Input from '../components/Input.js';
import PopupBtn from '../components/PopupButton.js';
import Loading from './Loading';

let styleSheet = {
  background: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)'
  },
  content: {
    position: 'absolute',
    height: '70%',
    right: '50px',
    left: '50px',
    top: '100px',
    borderRadius: '10px',
    backgroundColor:'#EBEBEB',
    overflow: 'hidden'
  },
  input: {
    top: '20px'
  },
  btnSignUp: {
    position: 'absolute',
    width: '30%',
    height: '60px',
    bottom: '40px',
    left: '35%',
    color: '#FFFFFF',
    backgroundColor: '#59C1CB',
    ':active': {
      backgroundColor: '#42929A',
    }
  },
  btnGetCode: {
    position: 'relative',
    width: 'auto',
    height: 'auto',
    color: '#FFFFFF',
    backgroundColor: '#59C1CB',
    ':active': {
      backgroundColor: '#42929A',
    }
  }
};

class SignUp extends React.Component {
    render() {
        if (this.props.isRegistering) {
            return (<Loading></Loading>);
        }
        if (this.props.verifyCodeExpireTime > 0) {
            setTimeout(function () {
                this.props.decrementVerifyCodeExpireTime();
            }.bind(this), 1000);
        }
        return (
            <div style={styleSheet.background}>
                <div style={styleSheet.content}>
                    <PopupTitle>Sign up</PopupTitle>
                    <CloseBtn onButton={hashHistory.goBack}></CloseBtn>
                    <Input ref="mobile" style={styleSheet.input} placeholder="請輸入手机"></Input>
                    <Input ref="verifyCode" style={styleSheet.input} placeholder="验证码"></Input>
                    <PopupBtn style={styleSheet.btnGetCode} onButton={this.onGetVerifyCode.bind(this)}>
                        {this.props.verifyCodeExpireTime===0?'获取验证码':this.props.verifyCodeExpireTime}
                    </PopupBtn>
                    <Input ref="password" style={styleSheet.input} placeholder="請輸入密碼"></Input>
                    <PopupBtn style={styleSheet.btnSignUp} onButton={this.onSignUp.bind(this)}>Sign up</PopupBtn>
                </div>
            </div>
        );
    }
    onClose() {
        this.props.onClose();
    }
    onSignUp() {
        let mobile = this.refs.mobile.getText();
        let verifyCode = this.refs.verifyCode.getText();
        let password = this.refs.password.getText();
        this.props.onSignUp(mobile, password, verifyCode);
    }
    onGetVerifyCode() {
        if (this.props.verifyCodeExpireTime > 0) {
            return;
        }
        let mobile = this.refs.mobile.getText();
        this.props.onGetVerifyCode(mobile);
    }
}
const mapStateToProps = (state, props) => {
    return {
        verifyCodeExpireTime: state.Auth.verifyCodeExpireTime,
        isRegistering: state.Auth.isRegistering,
        isAuthenticated: state.Auth.isAuthenticated,
        statusText: state.Auth.statusText
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSignUp: (mobile, password, verifyCodeExpireTime) => {
            dispatch(registerUser(mobile, password, verifyCodeExpireTime));
        },
        onClose: () => {
            dispatch(hide(null, null));
        },
        onGetVerifyCode: (mobile) => {
            dispatch(registerGetVerifyCode(mobile));
        },
        decrementVerifyCodeExpireTime: () => {
            dispatch(decrementVerifyCodeExpireTime());
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Radium(SignUp));
