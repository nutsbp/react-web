import React from 'react';
import Radium from 'radium';
import { connect } from 'react-redux';

import Main from './Main';
import Banner from './Banner';
import Message from './Message';
import Loading from './Loading';
import EDITOR_MAIN from './editor/Main';

var styleSheet = {
  wrap: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#EBEBEB'
  }
};
import { loadTemplate } from '../helpers/ui';

class Root extends React.Component {
    componentDidMount () {
        // TODO: loadTemplate 移植
        // let html;
        // let data = {
        //     template_id: 1,
        //     template_step: 1,
        //     templateConfig: {"1":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}},"2":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}},"3":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}},"4":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}},"5":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}},"6":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}},"7":{"1":{"layout":"unset"},"2":{"layout":"unset","count":4},"3":{"layout":"unset","count":4},"4":{"layout":"unset"},"5":{"layout":"unset","count":4},"6":{"layout":"unset","count":4},"7":{"layout":"unset","count":4},"8":{"layout":"unset","count":4},"9":{"layout":"unset","count":4},"10":{"layout":"unset","count":4}}},
        //     itemCount: 1
        // }
        // loadTemplate(data, (r) => {
        //  this.setState({
        //      page: r.innerHTML
        //  });
        // });
    }
  render() {
    if(this.props.frame === 'Editor') {
      styleSheet.wrap.overflow = 'hidden';
    }
    // if (this.state.page) {
    //     return (
    //         <div dangerouslySetInnerHTML={{__html: this.state.page}} />
    //     );
    // }
    return (
      <div style={styleSheet.wrap}>
        <Main></Main>
        <Banner></Banner>
        <Message display={this.props.frame} msg={this.props.msg}></Message>
        <Loading display={this.props.frame} msg={this.props.msg}></Loading>
        {this.props.children}
      </div>
    );
  }
}
const uiStateToProps = (state, props) => {
  var UI = state.UI.data;
  return {
    frame: UI.key,
    msg: UI.msg
  };
};
export default connect(uiStateToProps)(Radium(Root));
