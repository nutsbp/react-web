'use strict';
import React from 'react';
import Radium from 'radium';
// import { connect } from 'react-redux';

import NutsbpIntro from '../components/NutsbpIntro.js';
import ToolIntro from '../components/ToolIntro.js';

var styles = {
  wrap: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#EBEBEB'
  }
};

class Main extends React.Component {
  constructor() {
    super();
  }

  render() {
    // Basic main layout
    return (
      <div style={styles.wrap}>
        <NutsbpIntro></NutsbpIntro>
        <ToolIntro></ToolIntro>
      </div>
    );
  }
}

// const mapStateToProps = (state, props) => {
//   return {
//     result: JSON.stringify(state.test)
//   };
// };
//
// const mapDispatchToProps = (dispatch) => {
//   return {
//     onClick: () => {
//       dispatch(fetchSlide());
//     }
//   };
// };
// connect(mapStateToProps, mapDispatchToProps)(Radium(Main))
export default Radium(Main);
