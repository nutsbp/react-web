'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';

import Sidebar from './Sidebar.js';
import Toolbar from './Toolbar.js';
import SlideOptions from './SlideOptions.js';
import Slide from './Slide.js';


var styleSheet = {
  wrap: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#EBEBEB'
  },
  toolbar: {
    left: '70px'
  },
  slideOptions: {
    left: '260px'
  }
};

class Editor extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div style={styleSheet.wrap}>
        <Sidebar></Sidebar>
        <Toolbar style={styleSheet.toolbar}></Toolbar>
        <Slide></Slide>
        <SlideOptions style={styleSheet.slideOptions}></SlideOptions>
      </div>
    );
  }
}

export default connect()(Radium(Editor));
