'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';

import EditorButton from '../components/EditorButton.js';

var styleSheet = {
  sidebar: {
    position: 'absolute',
    width: '70px',
    height: '100%',
    backgroundColor: '#303030'
  },
  btn: {
    fontFamily: 'kustyle',
    borderBottom: '1px solid #393838'
  }
};

class Sidebar extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div style={styleSheet.sidebar}>
        <EditorButton style={styleSheet.btn}>&#xe822;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe80f;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe80c;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe811;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe812;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe813;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe814;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe815;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe816;</EditorButton>
        <EditorButton style={styleSheet.btn}>&#xe817;</EditorButton>
      </div>
    );
  }
}

export default connect()(Radium(Sidebar));
