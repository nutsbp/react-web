'use strict';
import React from 'react';
import Radium from 'radium';

import { connect } from 'react-redux';

import ToolButtons from './ToolButtons.js';

var styleSheet = {
  wrap: {
    position: 'absolute',
    width: '170px',
    height: '100%',
    backgroundColor: '#393838'
  }
};

class Toolbar extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div style={[ styleSheet.wrap, this.props.style ]}>
        <ToolButtons></ToolButtons>
      </div>
    );
  }
}

export default connect()(Radium(Toolbar));
