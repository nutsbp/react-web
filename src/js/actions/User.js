import Config from '../Config';
import constants from '../constants';
import { hashHistory } from 'react-router';
import {
    GET, POST,

    API_PERSONAL_MYPROFILE,
    API_UPDATE_PROFILE
} from '../middleware/APIs';
import { show, hide } from '../actions/UI.js';

/**
 * fetchProfile : 取得个人资料
 * updateProfile : 更新个人资料
 **/

let testdata = {};

export function fetchProfileRequest () {
    return {
        type: constants.get('FETCH_PROFILE_REQUEST')
    };
}

export function fetchProfileSuccess(data) {
    return {
        type: constants.get('FETCH_PROFILE_SUCCESS'),
        data: data
    };
}

export function fetchProfileFailure(error) {
    return {
        type: constants.get('FETCH_PROFILE_FAILURE'),
    };
}

export function updateProfileRequest () {
    return {
        type: constants.get('UPDATE_PROFILE_REQUEST')
    };
}

export function updateProfileSuccess(data) {
    return {
        type: constants.get('UPDATE_PROFILE_SUCCESS'),
        data: data
    };
}

export function updateProfileFailure(error) {
    return {
        type: constants.get('UPDATE_PROFILE_FAILURE'),
    };
}

export function fetchProfile () {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"","data":{"profile":{"unis":"7zhgandvg0ws","name":"小萌","avatar":{"width":291,"height":291,"format":"jpg","url":"http://img.bp.nutsb.com/avatar/for-wg3oe6nyishj.jpg"},"card":{"width":291,"height":291,"format":"jpg","url":"http://img.bp.nutsb.com/card/for-wg3oe6nyishj.jpg"},"province":"福建","city":"厦门","brief":"90后萌妹子","company":"有萌餐饮","position":"卖萌"},"contact":{"phone":"11166666666","email":"contactor@gmail.com","weixin":"dafd"}}};
        return dispatch => {
            dispatch(fetchProfileRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchProfileSuccess(testdata));
                }
                return dispatch(fetchProfileSuccess());
            }, 1000);
        };
    }
    return (dispatch) => {
        dispatch(fetchProfileRequest());
        GET(API_PERSONAL_MYPROFILE, {
            operation: 'get'
        }, (res) => {
            dispatch(fetchProfileSuccess(res.data));
        }, function (err) {
            dispatch(fetchProfileFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

/**
 * [updateProfile description]
 * @param  {[type]} data [avatar_id / name / phone / email / weixin ]
 * @return {[type]}      [description]
 */
export function updateProfile (data) {
    if (Config.get('test')) {
        testdata = data;
        return dispatch => {
            dispatch(updateProfileRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(updateProfileSuccess(testdata));
                }
                return dispatch(updateProfileFailure());
            }, 1000);
        };
    }
    data.operation = 'update';
    return (dispatch) => {
        dispatch(updateProfileRequest());
        GET(API_UPDATE_PROFILE, data, (res) => {
            dispatch(updateProfileSuccess(data));
        }, function (err) {
            dispatch(updateProfileFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}