import Config from '../Config';
import constants from '../constants';
import { hashHistory } from 'react-router';
import { GET, POST, API_LOGIN, API_VERIFICATION } from '../middleware/APIs';

let testdata = {};
/**
 * ╔═══════╗  ╔════════════════╗  ╔═════════════════╗
 * ║ 注册  ║─>║ 获取手机验证码  ║->║注册成功(->登入)  ║
 * ║       ║  ║                ║  ║或 失败(->登出   ║
 * ╚═══════╝  ╚════════════════╝  ╚═════════════════╝
 * ╔═══════╗  ╔═════════════════╗  ╔═══════════════╗
 * ║ 登入  ║─>║ 手机 & 密码验证  ║->║登入成功       ║
 * ║       ║  ║                 ║  ║或 失败(->登出 ║
 * ╚═══════╝  ╚═════════════════╝  ╚═══════════════╝
 **/

/**
 * registerGetVerifyCode : 取得手机注册验证码
 * loginUser: 手机登入
 * registerUser: 手机注册
 */

export function loginUserSuccess(data) {
    //localStorage.setItem('token', token);
    return {
        type: constants.get('LOGIN_USER_SUCCESS'),
        data: data
    };
}

export function loginUserFailure(error) {
    //localStorage.removeItem('token');
    return {
        type: constants.get('LOGIN_USER_FAILURE'),
        payload: {
            status: error.code,
            statusText: error.msg
        }
    };
}

export function loginUserRequest() {
  return {
    type: constants.get('LOGIN_USER_REQUEST')
  };
}

export function logout() {
    //localStorage.removeItem('token');
    return {
        type: constants.get('LOGOUT_USER')
    };
}

export function logoutAndRedirect() {
    return (dispatch, state) => {
        dispatch(logout());
        hashHistory.push('/');
    };
}

export function registerUserSuccess(data) {
    return {
        type: constants.get('REGISTER_USER_SUCCESS'),
        data: data
    };
}

export function registerUserFailure(error) {
    //localStorage.removeItem('token');
    return {
        type: constants.get('REGISTER_USER_FAILURE'),
        payload: {
            status: error.code,
            statusText: error.msg
        }
    };
}

export function registerUserRequest() {
    return {
        type: constants.get('REGISTER_USER_REQUEST')
    };
}

export function registerGetVerifyCode(mobile, redirect="/") {
    if (Config.get('test')) {
        return dispatch => {
            dispatch({
                type: constants.get('REGISTER_GET_VERIFY_CODE'),
                verifyCodeExpireTime: 60
            });
        };
    }
    return dispatch => {
        dispatch({
            type: constants.get('REGISTER_GET_VERIFY_CODE'),
            verifyCodeExpireTime: 60
        });
        POST(API_VERIFICATION, {
            operation: 'register',
            mobile: mobile
        }, function () {
            dispatch();
        }, function () {
            
        });
    };
}

export function decrementVerifyCodeExpireTime () {
    return {
        type: constants.get('DECREMENT_VERIFY_CODE')
    };
}

export function loginUser(mobile, password, redirect="/") {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"","data":{"profile":{"unis":"igdtvxbiw0wv","name":"","province":"","city":"","brief":"","company":"","position":""},"contact":{"phone":"15080333461","email":"","weixin":""},"credentials":{"role":1,"key":"mQM0ltcKuVytLv1z"},"project":{"id":160}}};
        return dispatch => {
            dispatch(loginUserRequest());
            setTimeout(() => {
                dispatch(loginUserSuccess(testdata.data));
                hashHistory.push('/');
            }, 1000);
        };
    }
    return dispatch => {
        dispatch(loginUserRequest());
        POST(API_LOGIN, {
            operation: 'login',
            mobile: mobile,
            password: password
        }, function (res) {
            dispatch(loginUserSuccess(res.data));
            hashHistory.push('/');
        }, function (resOrError) {
            dispatch(loginUserFailure(resOrError));
        });
    };
}

export function registerUser(mobile, password, verifyCode, role, redirect="/") {
    role = role || 1;
    if (Config.get('test')) {
        return function(dispatch) {
            dispatch(registerUserRequest());
            setTimeout(() => {
                dispatch(registerUserSuccess());
                hashHistory.push('/');
            }, 1000);
        };
    }
    return dispatch => {
        dispatch(registerUserRequest());
        POST(API_LOGIN, {
            operation: 'register',
            mobile: mobile,
            password: password,
            verify_code: verifyCode,
            role: role
        }, function (res) {
            dispatch(registerUserSuccess(res));
            hashHistory.push('/');
        }, function (resOrError) {
            dispatch(registerUserFailure(resOrError));
        });
    };
}