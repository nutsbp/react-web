import Config from '../Config';
import constants from '../constants';
import { hashHistory } from 'react-router';
import {
    GET, POST, PATCH,

    API_RESTFUL_SLIDE
} from '../middleware/APIs';
import { show, hide } from '../actions/UI.js';

/**
 * fetchSlide : 取得 Slide 详细资料
 * fetchSlideList : 取得 所有 Slide list
 * createSlide: 创建 Slide
 * updateSlide: 更新 Slide
 */

let testdata;

export function fetchSlideRequest () {
    return {
        type: constants.get('FETCH_SLIDE_REQUEST')
    };
}

export function fetchSlideSuccess(data) {
    return {
        type: constants.get('FETCH_SLIDE_SUCCESS'),
        data: data
    };
}

export function fetchSlideFailure(error) {
    return {
        type: constants.get('FETCH_SLIDE_FAILURE')
    };
}

export function fetchSlideListRequest () {
    return {
        type: constants.get('FETCH_SLIDELIST_REQUEST')
    };
}

export function fetchSlideListSuccess(list) {
    return {
        type: constants.get('FETCH_SLIDELIST_SUCCESS'),
        list: list
    };
}

export function fetchSlideListFailure(error) {
    return {
        type: constants.get('FETCH_SLIDELIST_FAILURE')
    };
}

export function createSlideRequest () {
    return {
        type: constants.get('CREATE_SLIDE_REQUEST')
    };
}

export function createSlideSuccess(data) {
    return {
        type: constants.get('CREATE_SLIDE_SUCCESS'),
        data: data
    };
}

export function createSlideFailure(error) {
    return {
        type: constants.get('UPDATE_SLIDE_FAILURE')
    };
}

export function updateSlideRequest () {
    return {
        type: constants.get('UPDATE_SLIDE_REQUEST')
    };
}

export function updateSlideSuccess(data) {
    return {
        type: constants.get('UPDATE_SLIDE_SUCCESS'),
        data: data
    };
}

export function updateSlideFailure(error) {
    return {
        type: constants.get('UPDATE_SLIDE_FAILURE')
    };
}

export function fetchSlide (id) {
    if (!id) {
        return;
    }
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"成功","data":[{"slide_id":1,"unis":"000000000001","privacy_id":1,"finished_step":0,"create_time":"2016-05-13T15:34:10.000Z","update_time":"2016-07-07T15:03:21.000Z"},{"slide_id":2,"unis":"000000000002","privacy_id":1,"finished_step":0,"create_time":"2016-05-13T15:34:18.000Z","update_time":"2016-05-13T15:34:18.000Z"}]};
        return dispatch => {
            dispatch(fetchSlideRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchSlideSuccess(testdata.data));
                }
                dispatch(fetchSlideFailure());
            }, 1000);
        };
    }
    let url = API_RESTFUL_SLIDE;
    url += '/' + id;

    return (dispatch) => {
        dispatch(fetchSlideRequest());
        GET(url, {}, (res) => {
            dispatch(fetchSlideSuccess(res.data));
        }, (err) => {
            dispatch(fetchSlideFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

export function fetchSlideList () {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"成功","data":[{"slide_id":1,"unis":"000000000001","privacy_id":1,"finished_step":0,"create_time":"2016-05-13T15:34:10.000Z","update_time":"2016-07-07T15:03:21.000Z"},{"slide_id":2,"unis":"000000000002","privacy_id":1,"finished_step":0,"create_time":"2016-05-13T15:34:18.000Z","update_time":"2016-05-13T15:34:18.000Z"}]};
        return dispatch => {
            dispatch(fetchSlideListRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchSlideListSuccess(testdata.data));
                }
                dispatch(fetchSlideListFailure());
            }, 1000);
        };
    }
    let url = API_RESTFUL_SLIDE;

    return (dispatch) => {
        dispatch(fetchSlideListRequest());
        GET(url, {}, (res) => {
            dispatch(fetchSlideListSuccess(res.data));
        }, (err) => {
            dispatch(fetchSlideListFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

export function createSlide (data) {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"成功","data":{"slide_id":333,"unis":"3wyrr58g8lxm","bp_id":160}};
        return dispatch => {
            dispatch(createSlideRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(createSlideSuccess(testdata.data));
                }
                dispatch(createSlideFailure());
            }, 1000);
        };
    }
    data = data || {};
    let url = API_RESTFUL_SLIDE;

    return (dispatch) => {
        dispatch(createSlideRequest());
        POST(url, data, (res) => {
            dispatch(createSlideSuccess(res.data));
        }, (err) => {
            dispatch(createSlideFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

export function updateSlide (id, data) {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"成功","data":{"slide_id":333,"template_id":"3","data":"","update_time":"2016-07-12 15:11:34"}};
        return dispatch => {
            dispatch(updateSlideRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(updateSlideSuccess(testdata.data));
                }
                dispatch(updateSlideFailure());
            }, 1000);
        };
    }
    if (!id) {
        return;
    }
    data = data || {};
    let url = API_RESTFUL_SLIDE;
    url += '/' + id;

    return (dispatch) => {
        dispatch(updateSlideRequest());
        PATCH(url, data, (res) => {
            dispatch(updateSlideSuccess(res.data));
        }, (err) => {
            dispatch(updateSlideFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

export function fetchSlideWrap (info) {
  return {
    type: constants.get('FETCH_SLIDE_WRAP'),
    data: info
  };
}
