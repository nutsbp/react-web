import Config from '../Config';
import constants from '../constants';
import { hashHistory } from 'react-router';
import {
    GET, POST, PATCH,

    API_RESTFUL_GET_PROJECT
} from '../middleware/APIs';
import { show, hide } from '../actions/UI.js';

/**
 * fetchProject: 取得项目
 */

let testdata;

export function fetchProjectRequest () {
    return {
        type: constants.get('FETCH_PROJECT_REQUEST')
    };
}

export function fetchProjectSuccess(data) {
    return {
        type: constants.get('FETCH_PROJECT_SUCCESS'),
        data: data
    };
}

export function fetchProjectFailure(error) {
    return {
        type: constants.get('FETCH_PROJECT_FAILURE')
    };
}

export function fetchProject () {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"成功","data":[{"bp_id":160,"unis":"donfzxcxksvd","name":"測試BP","owner_user_id":10250,"privacy_id":2,"verify_id":1,"limit_time":0,"user_id":10250,"finished_count":11,"finished_modules":"1,2,4,6,7,9,10,11,12,13,3","finished_step":0,"cover_id":1685,"industry_id":5,"country_id":1,"province_id":14,"city_id":116,"address":"","financing_status":"waiting","financing_stage":"seed","introduction":"test bp test bp test","create_user_id":10250,"create_time":"2016-05-02T10:59:34.000Z","update_time":"2016-05-04T17:09:16.000Z","PRODUCT":["@@@@@@@TEST 产品介绍@@@@@@@"],"MARKET":["市场规模 *市场规模 *市场规模 *v"],"OPERATION":["运营数据 *运营数据 *运营数据 *"],"GOAL":["运营计划 *运营计划 *运营计划 *运营计划 *"],"FINANCE":["融资规划 *融资规划 *融资规划 *融资规划 *"],"url":[{"id":97,"bp_id":160,"block_id":6,"url":"http://www.bp.nutsb.com/","create_time":"2016-05-02T11:02:31.000Z"}],"milestone":[{"id":157,"bp_id":160,"content":"milestoen 1","event_time":"2016-05-04T00:00:00.000Z","create_time":"2016-05-04T17:09:16.000Z"}],"slide":[{"slide_id":1,"unis":"000000000001","privacy_id":1,"finished_step":0,"create_time":"2016-05-13T15:34:10.000Z","update_time":"2016-07-12T15:06:48.000Z"},{"slide_id":2,"unis":"000000000002","privacy_id":1,"finished_step":0,"create_time":"2016-05-13T15:34:18.000Z","update_time":"2016-05-13T15:34:18.000Z"},{"slide_id":333,"unis":"3wyrr58g8lxm","privacy_id":2,"template_id":3,"finished_step":0,"create_time":"2016-07-12T15:08:55.000Z","update_time":"2016-07-12T15:11:34.000Z"}],"count":{"bp_id":160,"view_count":46,"view_daily_max1_count":5,"builtin_folder_id_1_count":0,"builtin_folder_id_2_count":0,"builtin_folder_id_3_count":0,"builtin_folder_id_4_count":0},"funding_usage":[],"tag":[],"image":[],"team":[{"id":186,"bp_id":160,"name":"Justin","duty_id":144,"introduction":"Yes,\nI'm tester.\n@@","create_time":"2016-05-02T11:01:47.000Z","duty":"RD"}],"user":{"unis":"igdtvxbiw0wv","play_role":1,"introduction":""},"financing":{"financing_amount":999},"pain_points":[{"desc":"@@@@@@@TEST 从事@@@@@@@"}],"solution":[{"desc":"@@@@@@@TEST 痛点@@@@@@@"}],"competition":[{"desc":"@@@@@@@TEST 竞品@@@@@@@"}],"competitive_edge":[{"desc":"@@@@@@@TEST 竞争优势@@@@@@@"}],"profit_model":[{"desc":"盈利模式 *盈利模式 *盈利模式 *"}]}]};
        return dispatch => {
            dispatch(fetchProjectRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchProjectSuccess(testdata.data));
                }
                dispatch(fetchProjectFailure());
            }, 1000);
        };
    }
    let url = API_RESTFUL_GET_PROJECT;

    return (dispatch) => {
        dispatch(fetchProjectRequest());
        GET(url, {}, (res) => {
            dispatch(fetchProjectSuccess(res.data));
        }, function (err) {
            dispatch(fetchProjectFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}
