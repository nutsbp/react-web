import Config from '../Config';
import constants from '../constants';
import {
  hashHistory
} from 'react-router';
import {
    GET, POST, PATCH,
    
    API_GET_BANNERLIST,
    API_GET_BP_BRIEF,
    API_SET_SHARE
} from '../middleware/APIs';
import { show, hide } from '../actions/UI.js';

/**
 * fetchBpBanner : 取得 bp banner (public)
 * fetchBpBrief : 取得 bp 摘要
 * fetchBpShareLink : 取得 bp 分享连结
 */

let testdata = {};


export function fetchBpBannerRequest () {
    return {
        type: constants.get('FETCH_BP_BANNER_REQUEST')
    };
}

export function fetchBpBannerSuccess(data) {
    return {
        type: constants.get('FETCH_BP_BANNER_SUCCESS'),
        banner: data
    };
}

export function fetchBpBannerFailure(error) {
    return {
        type: constants.get('FETCH_BP_BANNER_FAILURE')
    };
}

export function fetchBpBriefRequest () {
    return {
        type: constants.get('FETCH_BP_BRIEF_REQUEST')
    };
}

export function fetchBpBriefSuccess(data) {
    return {
        type: constants.get('FETCH_BP_BRIEF_SUCCESS'),
        brief: data
    };
}

export function fetchBpBriefFailure(error) {
    return {
        type: constants.get('FETCH_BP_BRIEF_FAILURE')
    };
}

export function fetchBpShareLinkRequest () {
    return {
        type: constants.get('FETCH_BP_SHARELINK_REQUEST')
    };
}

export function fetchBpShareLinkSuccess(data) {
    return {
        type: constants.get('FETCH_BP_SHARELINK_SUCCESS'),
        sharelink: data
    };
}

export function fetchBpShareLinkFailure(error) {
    return {
        type: constants.get('FETCH_BP_SHARELINK_FAILURE')
    };
}

/**
 * @ref http://justin.nutsb.com:1189/api/nutsbp/wikis/v1.0-uipage-pioneer-mybp-banner
 */
export function fetchBpBanner() {
    if (Config.get('test')) {
        testdata = {"code":0,"msg":"","data":{"banner":[{"image":"http:\/\/www.bp.nutsb.com\/html\/app\/home\/slides\/11.png","link":"https:\/\/mp.weixin.qq.com\/s?__biz=MzIxMDEyODA3Mw==&mid=405890627&idx=1&sn=4dad521c6e0455e03d670f119a2c2dd0"},{"image":"http:\/\/www.bp.nutsb.com\/html\/app\/home\/slides\/12.png","link":"http:\/\/www.nutsbp.com\/html\/how_to\/how_to.html"}]}};
        return dispatch => {
            dispatch(fetchBpBannerRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchBpBannerSuccess(testdata.data));
                }
                dispatch(fetchBpBannerFailure());
            }, 1000);
        };
    }
    return dispatch => {
        dispatch(fetchBpBannerRequest());
        GET(API_GET_BANNERLIST, {}, (res) => {
            dispatch(fetchBpBannerSuccess(res.data));
        }, (err) => {
            dispatch(fetchBpBannerFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

/**
 * @ref http://justin.nutsb.com:1189/api/nutsbp/wikis/v1.0-uipage-pioneer-mybp-myBpPage
 */
export function fetchBpBrief() {
    if (Config.get('test')) {
        testdata = {data: {}};
        return dispatch => {
            dispatch(fetchBpBriefRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchBpBriefSuccess(testdata.data));
                }
                dispatch(fetchBpBriefFailure());
            }, 1000);
        };
    }
    return dispatch => {
        dispatch(fetchBpBriefRequest());
        GET(API_GET_BP_BRIEF, {}, (res) => {
            dispatch(fetchBpBriefSuccess(res.data));
        }, (err) => {
            dispatch(fetchBpBriefFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

/**
 * @ref http://justin.nutsb.com:1189/api/nutsbp/wikis/v1.0-pioneer-mybp-bpShareLink
 */
export function fetchBpShareLink() {
    if (Config.get('test')) {
        testdata = {data: {}};
        return dispatch => {
            dispatch(fetchBpShareLinkRequest());
            setTimeout(() => {
                if (Math.round(Math.random())) {
                    return dispatch(fetchBpShareLinkSuccess(testdata.data));
                }
                dispatch(fetchBpShareLinkFailure());
            }, 1000);
        };
    }
    return dispatch => {
        dispatch(fetchBpShareLinkRequest());
        GET(API_SET_SHARE, {
            operation: 'get'
        }, (res) => {
            dispatch(fetchBpShareLinkSuccess(res.data));
        }, (err) => {
            dispatch(fetchBpShareLinkFailure());
            dispatch(show('Message', err.msg || " "));
        });
    };
}

// TODO 开启 / 关闭分享连结的 action
export function openBpShareLink() {
    return dispatch => {
        GET(API_SET_SHARE, {
            operation: 'open'
        }, (res) => {
        }, (err) => {
        });
    };
}
export function closeBpShareLink() {
    return dispatch => {
        GET(API_SET_SHARE, {
            operation: 'close'
        }, (res) => {
        }, (err) => {
        });
    };
}