/**
 * UI 操作
 **/
const SHOW = 'SHOW';
const HIDE = 'HIDE';


export function show (key, msg) {
  return {
    type: SHOW,
    data: {
      key, msg
    }
  };
}

export function hide (key, msg) {
  return {
    type: HIDE,
    data: {
      key, msg
    }
  };
}