import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import routes from './routes';
import configureStore from './store/configureStore';

import NUTS from './libs/NUTS';
import Config from './Config';

const store = configureStore();
const rootElement = document.getElementById('root');

// config set:
let l = window.location;
let argv;
let ENV;
if (l) {
    argv = NUTS.parseQueryString(l.search);
    if (argv.debug !== undefined) {
      ENV = NUTS.DEV;
    } else if (argv.production !== undefined) {
        ENV = NUTS.PROD;
    } else if (l && (l.host === 'localhost' || l.host.match('127.0.0.1'))) {
        ENV = NUTS.DEV;
    } else {
        ENV = NUTS.PROD;
    }
} else {
    ENV = NUTS.DEV;
}
Config.set({
    ENV: ENV,
    argv: argv,
    test: argv.test === ""
});
store.dispatch({
    type: 'SET_CONFIG',
    data: {
        ENV: ENV,
        argv: argv,
        test: argv.test === ""
    }
});

// 有在 provider 設定 store 後，之後的 containers 都會有 dispatch 可以使用
render(
    <Provider store={store}>
        {routes}
    </Provider>,
    rootElement
);

//                           _ooOoo_
//                          o8888888o
//                          88" . "88
//                          (| -_- |)
//                           O\ = /O
//                        ____/`—'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\—/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /—.—\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - `: | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=—='
//
//         ………………………………………
//                  佛祖保佑             永无BUG
//          佛曰:
//                  写字楼里写字间，写字间里程序员；
//                  程序人员写程序，又拿程序换酒钱。
//                  酒醒只在网上坐，酒醉还来网下眠；
//                  酒醉酒醒日复日，网上网下年复年。
//                  但愿老死电脑间，不愿鞠躬老板前；
//                  奔驰宝马贵者趣，公交自行程序员。
//                  别人笑我忒疯癫，我笑自己命太贱；
//                  不见满街漂亮妹，哪个归得程序员？
