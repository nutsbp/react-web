import { createReducer } from '../helpers/utils';
import constants from '../constants';

// TODO 设计 state
const initialState = {
    isFetching: false,
    isFetched: false,
    isUpdating: false,
    isUpdated: false,
    bp: null,
    slides: null
};

export default createReducer(initialState, {
    [constants.get('FETCH_BP_BANNER_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isFetching: true,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_BANNER_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            banner: action.banner,
            isFetching: false,
            isFetched: true,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_BANNER_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            banner: null,
            isFetching: false,
            isFetched: false,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_BRIEF_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isFetching: true,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_BRIEF_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            brief: action.brief,
            isFetching: false,
            isFetched: true,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_BRIEF_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            brief: null,
            isFetching: false,
            isFetched: false,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_SHARELINK_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isFetching: true,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_SHARELINK_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            sharelink: action.sharelink,
            isFetching: false,
            isFetched: true,
            statusText: null
        });
    },
    [constants.get('FETCH_BP_SHARELINK_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            sharelink: null,
            isFetching: false,
            isFetched: false,
            statusText: null
        });
    },
});
