import { createReducer } from '../helpers/utils';

const SHOW = 'SHOW';
const HIDE = 'HIDE';

const initialState = {
    data: {
        key: null,
        msg: null
    }
};

export default createReducer(initialState, {
    [SHOW]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data
        });
    },
    [HIDE]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data
        });
    }
});
