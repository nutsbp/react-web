import { createReducer } from '../helpers/utils';
import constants from '../constants';

const initialState = {
    isFetching: false,
    isFetched: false,
    isCreating: false,
    isCreated: false,
    isUpdating: false,
    isUpdated: false,
    isReady: false,
    data: null,
    list: null,
    statusText: null
};

export default createReducer(initialState, {
    [constants.get('FETCH_SLIDE_WRAP')]: (state, action) => {
      return Object.assign({}, state, {
          isFetching: true,
          isReady: true,
          data: action.data
      });
    },
    [constants.get('FETCH_SLIDE_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isFetching: true,
            statusText: null
        });
    },
    [constants.get('FETCH_SLIDE_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data,
            isFetching: false,
            isFetched: true,
            statusText: null
        });
    },
    [constants.get('FETCH_SLIDE_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isFetching: false,
            isFetched: false,
            statusText: null
        });
    },
    [constants.get('FETCH_SLIDELIST_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isFetching: true,
            statusText: null
        });
    },
    [constants.get('FETCH_SLIDELIST_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            list: action.list,
            isFetching: false,
            isFetched: true,
            statusText: null
        });
    },
    [constants.get('FETCH_SLIDELIST_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isFetching: false,
            isFetched: false,
            statusText: null
        });
    },
    [constants.get('CREATE_SLIDE_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isCreating: true,
            statusText: null
        });
    },
    [constants.get('CREATE_SLIDE_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data,
            isCreating: false,
            isCreated: true,
            statusText: null
        });
    },
    [constants.get('CREATE_SLIDE_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isCreating: false,
            isCreated: false,
            statusText: null
        });
    },
    [constants.get('UPDATE_SLIDE_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isUpdating: true,
            statusText: null
        });
    },
    [constants.get('UPDATE_SLIDE_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data,
            isUpdating: false,
            isUpdated: true,
            statusText: null
        });
    },
    [constants.get('UPDATE_SLIDE_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isUpdating: false,
            isUpdated: false,
            statusText: null
        });
    }
});
