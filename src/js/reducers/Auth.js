import { createReducer } from '../helpers/utils';
import constants from '../constants';

const initialState = {
    token: null,
    userName: null,
    isAuthenticated: false, // 已登入
    isAuthenticating: false, // 登入中
    isRegistering: false, // 注册中
    verifyCodeExpireTime: 0, // 验证码 倒数
    statusText: null
};

export default createReducer(initialState, {
    [constants.get('LOGIN_USER_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            'isAuthenticating': true,
            'statusText': null
        });
    },
    [constants.get('LOGIN_USER_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': true,
            'data': action.data,
            'statusText': 'You have been successfully logged in.'
        });

    },
    [constants.get('LOGIN_USER_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            'isAuthenticating': false,
            'isAuthenticated': false,
            'token': null,
            'userName': null,
            'data': null,
            'statusText': `Authentication Error: ${action.payload.status} ${action.payload.statusText}`
        });
    },
    [constants.get('LOGOUT_USER')]: (state, action) => {
        return Object.assign({}, state, {
            'isAuthenticated': false,
            'data': null,
            'token': null,
            'userName': null,
            'statusText': 'You have been successfully logged out.'
        });
    },
    [constants.get('REGISTER_USER_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            'isRegistering': true,
            'statusText': null
        });
    },
    [constants.get('REGISTER_USER_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            'isRegistering': false,
            'isAuthenticated': true,
            'data': action.data,
            // 'token': action.payload.token,
            // 'userName': jwtDecode(payload.token).userName,
            'statusText': 'You have been successfully logged in.'
        });

    },
    [constants.get('REGISTER_USER_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            'isRegistering': false,
            'isAuthenticated': false,
            'token': null,
            'userName': null,
            'statusText': `Authentication Error: ${action.payload.status} ${action.payload.statusText}`
        });
    },
    [constants.get('REGISTER_GET_VERIFY_CODE')]: (state, action) => {
        return Object.assign({}, state, {
            'verifyCodeExpireTime': 60,
        });
    },
    [constants.get('DECREMENT_VERIFY_CODE')]: (state, action) => {
        let verifyCodeExpireTime = parseInt(state.verifyCodeExpireTime) - 1;
        return Object.assign({}, state, {
            'verifyCodeExpireTime': verifyCodeExpireTime,
        });
    }
});
