import { createReducer } from '../helpers/utils';
import constants from '../constants';

const initialState = {
    isFetching: false,
    isFetched: false,
    isCreating: false,
    isCreated: false,
    isUpdating: false,
    isUpdated: false,
    data: null,
    statusText: null
};

export default createReducer(initialState, {
    [constants.get('FETCH_PROJECT_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isFetching: true,
            statusText: null
        });
    },
    [constants.get('FETCH_PROJECT_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data,
            isFetching: false,
            isFetched: true,
            statusText: null
        });
    },
    [constants.get('FETCH_PROJECT_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isFetching: false,
            isFetched: false,
            statusText: null
        });
    },
    [constants.get('CREATE_PROJECT_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isCreating: true,
            statusText: null
        });
    },
    [constants.get('CREATE_PROJECT_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data,
            isCreating: false,
            isCreated: true,
            statusText: null
        });
    },
    [constants.get('CREATE_PROJECT_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isCreating: false,
            isCreated: false,
            statusText: null
        });
    },
    [constants.get('UPDATE_PROJECT_REQUEST')]: (state, action) => {
        return Object.assign({}, state, {
            isUpdating: true,
            statusText: null
        });
    },
    [constants.get('UPDATE_PROJECT_SUCCESS')]: (state, action) => {
        return Object.assign({}, state, {
            data: action.data,
            isUpdating: false,
            isUpdated: true,
            statusText: null
        });
    },
    [constants.get('UPDATE_PROJECT_FAILURE')]: (state, action) => {
        return Object.assign({}, state, {
            data: null,
            isUpdating: false,
            isUpdated: false,
            statusText: null
        });
    }
});
