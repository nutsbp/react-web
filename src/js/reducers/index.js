import {
  combineReducers
} from 'redux';
import Auth from './Auth';
import Bp from './Bp';
import Config from './Config';
import Project from './Project';
import Slide from './Slide';
import UI from './UI';
import User from './User';

const nutsApp = combineReducers({
  Auth,
  Bp,
  Config,
  UI,
  Project,
  Slide,
  User
});

export default nutsApp;
