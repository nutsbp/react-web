import { createReducer } from '../helpers/utils';
import constants from '../constants';
import NUTS from '../libs/NUTS';

// move from nutsbp-editor
const initialState = {
    _cacheCss: null, // cache css file
    _cacheHtml: null, // cache html file
    _cacheJs: null, // cache js file
    NAME: 'NUTSEDITOR',
    VERSION: '1.0.0',
    root: root, // for nodejs export root scope
    ENV: NUTS.PROD,
    isInternalRelease: false, // for internal release.

    argv: {},

    /** dectect device */
    isMobile: false, // dectect is mobile
    isNative: false, // dectect is app
    isAndroid: false, // dectect is app & android
    isIos: false, // dectect is app & ios
    APPSTRING: /NUTSAPP/, // app useragent string

    /** 启用 JSBridge 部份情况要关掉 */
    enableJSBridge: false,

    /** Section in use */
    cssRoot: 'src/css/',
    htmlRoot: 'src/html/',
    scaleValue: [], // the view scale
    step: 1, // 大纲 当前步骤
    page: 1, // 当前页数
    pageLimit: 50, // 页数限制
    undoLimit: 30, // undo 次数限制 (每页)

    /** 大纲模式 config */
    outline: {
        maxStep: 10,
    },
    disableImageEdit: true, // 暂时 关闭 图片功能

    renderSize: { // 原始 render size (android拍照 回 web window.innerHeight 会跑掉)
        windowInnerWidth: 0,
        windowInnerHeight: 0
    }
};
export default createReducer(initialState, {
    [constants.get('SET_CONFIG')]: (state, action) => {
        let obj = {};
        let data = action.data;
        if (action.path) {
            NUTS.declare(action.path, data, obj);
        } else {
            obj = data;
        }
        return Object.assign({}, state, obj);
    }
});
